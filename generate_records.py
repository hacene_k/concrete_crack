# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Read CIFAR-10 data from pickled numpy arrays and writes TFRecords.

Generates tf.train.Example protos and writes them to TFRecord files from the
python version of the CIFAR-10 dataset downloaded from
https://www.cs.toronto.edu/~kriz/cifar.html.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import glob
import os
import sys

import tarfile
from six.moves import cPickle as pickle
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import numpy as np
import cv2


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _get_file_names(dataset_path):
    """Returns the file names expected to exist in the input_dir."""
    file_names = {}
    files = glob.glob(dataset_path + "/**", recursive=True)
    files = list(filter(lambda x: "jpg" in x.lower(), files))
    np.random.shuffle(files)
    file_names['train'] = files[:int(len(files) * 0.6)]
    file_names['validation'] = files[int(len(files) * 0.6):int(len(files) * 0.8)]
    file_names['eval'] = files[int(len(files) * 0.8):]
    return file_names


def read_pickle_from_file(filename):
    with tf.gfile.Open(filename, 'rb') as f:
        if sys.version_info >= (3, 0):
            data_dict = pickle.load(f, encoding='bytes')
        else:
            data_dict = pickle.load(f)
    return data_dict


def extract_label(input_path):
    if "CD" in input_path or "CP" in input_path or "CW" in input_path:
        label = 1
    elif "UD" in input_path or "UP" in input_path or "UW" in input_path:
        label = 0
    return label


def convert_to_tfrecord(files, output_file):
    """Converts a file to TFRecords."""
    print('Generating %s' % output_file)
    with tf.python_io.TFRecordWriter(output_file) as record_writer:
        for input_file in files:
            image = cv2.imread(input_file)[:, :, ::-1]
            data = cv2.imencode('.jpg', image)[1]
            label = extract_label(input_file)
            example = tf.train.Example(features=tf.train.Features(
                feature={
                    'image': _bytes_feature(data.tobytes()),
                    'label': _int64_feature(label)
                }))
            record_writer.write(example.SerializeToString())


def main(output_dir, dataset_path):
    # print('Download from {} and extract.'.format(CIFAR_DOWNLOAD_URL))
    # download_and_extract(data_dir)
    file_names = _get_file_names(dataset_path)
    # input_dir = os.path.join(data_dir, CIFAR_LOCAL_FOLDER)
    for mode, files in file_names.items():
        # input_files = [os.path.join(input_dir, f) for f in files]
        output_file = os.path.join(output_dir, mode + '.tfrecords')
        if not os.path.exists(os.path.dirname(output_file)):
            os.makedirs(os.path.dirname(output_file))
        try:
            os.remove(output_file)
        except OSError:
            pass
        # Convert to tf.train.Example and write the to TFRecords.
        convert_to_tfrecord(files, output_file)
    print('Done!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--output_dir',
        type=str,
        default='',
        help='Where to output tfrecords')
    parser.add_argument(
        '--dataset_path',
        type=str,
        default='',
        help='Directory to dataset')

    args = parser.parse_args()
    main(args.output_dir, args.dataset_path)
